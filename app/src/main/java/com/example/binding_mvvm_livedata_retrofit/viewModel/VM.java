package com.example.binding_mvvm_livedata_retrofit.viewModel;

import android.arch.core.util.Function;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.arch.lifecycle.ViewModel;

import com.example.binding_mvvm_livedata_retrofit.binding.DataModel;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling.CarriersItem;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling.ItinerariesItem;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling.LegsItem;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling.PollingResponse;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling.PricingOptionsItem;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling.SegmentsItem;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VM extends android.arch.lifecycle.ViewModel {
    public static final String FlightURL = "https://logos.skyscnr.com/images/airlines/favicon/EZ.png";

    public VM()
    {

    }

    public VM(String test1) {
        String test = "";
    }

    public LiveData<List<DataModel>> getPollingLiveData() {

        LiveData<List<DataModel>> dataModel = Transformations.switchMap(Repository.getInstance().getPollingSessionLiveData(), new Function<PollingResponse, LiveData<List<DataModel>>>() {
            @Override
            public LiveData<List<DataModel>> apply(PollingResponse input) {
                return convertPollingResponseToSkyDataModel(input);
            }
        });

        return dataModel;
    }

    public  LiveData<List<DataModel>> convertPollingResponseToSkyDataModel(PollingResponse response)
    {
        List<DataModel> list = new ArrayList<>();
        for (ItinerariesItem iternary:response.getItineraries()) {

            //OutBound data
            String outBoundAirportOrigin;
            String outBoundAirportDestination;
            String outBoundDepartureTime;
            String outBoundArrivalTime;
            String outBoundjourneyType;
            String outBoundJourneyDuration;
            String outBoundFlightURL = FlightURL;
            List<String> outBoundcarriers = new ArrayList<>();

            LegsItem legsOutItem = response.getLegTable().get(iternary.getOutboundLegId());

            for (int id:
                    legsOutItem.getSegmentIds()) {
                SegmentsItem segItem = response.getSegmentTable().get(id);
                if(segItem != null) {
                    CarriersItem carrier = response.getCarrierTable().get(segItem.getCarrier());
                    String name = carrier.getName();
                    outBoundFlightURL = carrier.getImageUrl();
                    outBoundcarriers.add(name);
                }
            }

            outBoundAirportOrigin = response.getPlaceTable().get(legsOutItem.getOriginStation()).getCode();
            outBoundAirportDestination = response.getPlaceTable().get(legsOutItem.getDestinationStation()).getCode();
            outBoundDepartureTime = getTimeFromDateTime(legsOutItem.getDeparture());
            outBoundArrivalTime = getTimeFromDateTime(legsOutItem.getArrival());
            outBoundjourneyType = legsOutItem.getSegmentIds().size() > 1? legsOutItem.getSegmentIds().size() + "Stops":"Non-stop";
            outBoundJourneyDuration = String.valueOf(legsOutItem.getDuration());



            DataModel.Outbound outbound = new DataModel.Outbound(outBoundAirportOrigin,outBoundAirportDestination,outBoundDepartureTime,
                    outBoundArrivalTime,outBoundjourneyType,outBoundJourneyDuration,outBoundFlightURL,outBoundcarriers);

            //InBoundData
            String inBoundAirportOrigin;
            String inBoundAirportDestination;
            String inBoundDepartureTime;
            String inBoundArrivalTime;
            String inBoundjourneyType;
            String inBoundJourneyDuration;
            String inBoundFlightURL = FlightURL;
            List<String> inBoundcarriers  = new ArrayList<>();

            LegsItem legsInItem = response.getLegTable().get(iternary.getInboundLegId());

            for (int id:
                    legsInItem.getSegmentIds()) {
                SegmentsItem segItem = response.getSegmentTable().get(id);
                CarriersItem carrier = response.getCarrierTable().get(segItem.getCarrier());
                String name = carrier.getName();
                inBoundFlightURL = carrier.getImageUrl();
                inBoundcarriers.add(name);
            }

            inBoundAirportOrigin = response.getPlaceTable().get(legsInItem.getOriginStation()).getCode();
            inBoundAirportDestination = response.getPlaceTable().get(legsInItem.getDestinationStation()).getCode();
            inBoundDepartureTime = getTimeFromDateTime(legsInItem.getDeparture());
            inBoundArrivalTime = getTimeFromDateTime(legsInItem.getArrival());
            inBoundjourneyType = legsInItem.getSegmentIds().size() > 1? legsInItem.getSegmentIds().size() + " Stops":"Non-stop";
            inBoundJourneyDuration = String.valueOf(legsInItem.getDuration());



            DataModel.Inbound inbound = new DataModel.Inbound(inBoundAirportOrigin,inBoundAirportDestination,inBoundDepartureTime,
                    inBoundArrivalTime,inBoundjourneyType,inBoundJourneyDuration,inBoundFlightURL,inBoundcarriers);



            for(PricingOptionsItem pricing:iternary.getPricingOptions())
            {
                String symbol = response.getCurrencies().size() > 0 ? response.getCurrencies().get(0).getSymbol():"";
                String tripcost = symbol + String.valueOf(pricing.getPrice());


                for(int agentsKey:pricing.getAgents())
                {
                    String agent = response.getAgentTable().get(agentsKey).getName();
                    DataModel dataModel = new DataModel(outbound,inbound,"10.0",tripcost,agent);
                    list.add(dataModel);
                }

            }
        }
        MutableLiveData<List<DataModel>> model = new MutableLiveData<List<DataModel>>();
        model.setValue(list);
        return model;
    }

    private String getTimeFromDateTime(String date)
    {
        SimpleDateFormat parseDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        SimpleDateFormat returnDateFormat = new SimpleDateFormat("HH:mm");
        try {
            Date dateTime = parseDateFormat.parse(date);

            return  returnDateFormat.format(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public LiveData getAPIStatus()
    {
        return Repository.getInstance().getAPIStatusData();
    }

    public void makeSessionRequest(String country,String currency,String locale,String locationSchema,String originplace,
                                   String destinationplace,String outbounddate, String inbounddate,int adults,int children,int infant,
                                   String cabinClass)
    {
        Repository.getInstance().makeSessionRequest(country,currency,locale,locationSchema, originplace,
                destinationplace, outbounddate, inbounddate, adults, children, infant,cabinClass);
    }

}
