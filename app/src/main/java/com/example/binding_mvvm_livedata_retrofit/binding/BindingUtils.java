package com.example.binding_mvvm_livedata_retrofit.binding;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.example.binding_mvvm_livedata_retrofit.ui.App;

public class BindingUtils {

    @BindingAdapter(value = {"imageUrl"})
    public static void setImageUrl(ImageView imageView, String imageUrl) {
        Glide.with(App.getAppContext())
                .load(imageUrl)
                .into(imageView);
    }

}
