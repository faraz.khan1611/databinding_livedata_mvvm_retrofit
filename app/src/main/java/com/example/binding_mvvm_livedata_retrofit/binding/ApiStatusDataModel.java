package com.example.binding_mvvm_livedata_retrofit.binding;

public class ApiStatusDataModel {
    Boolean isSuccess;
    String message;

    public ApiStatusDataModel(Boolean isSuccess, String message) {
        this.isSuccess = isSuccess;
        this.message = message;
    }

    public Boolean getSuccess() {
        return isSuccess;
    }

    public String getMessage() {
        return message;
    }
}
