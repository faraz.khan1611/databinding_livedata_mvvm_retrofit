package com.example.binding_mvvm_livedata_retrofit.binding;

import java.util.List;
import java.util.stream.Collectors;

public class DataModel {

    public static class Inbound
    {
        String inBoundAirportOrigin;
        String inBoundAirportDestination;
        String inBoundDepartureTime;
        String inBoundArrivalTime;
        String inBoundjourneyType;
        String inBoundJourneyDuration;
        List<String> inBoundcarriers;
        String inBoundFlightName;
        String inBoundFlightURL;

        public String getInBoundFlightURL() {
            return inBoundFlightURL;
        }

        public Inbound(String inBoundAirportOrigin, String inBoundAirportDestination, String inBoundDepartureTime, String inBoundArrivalTime,
                       String inBoundjourneyType, String inBoundJourneyDuration, String inBoundFlightURL,List<String> inBoundcarriers) {
            this.inBoundAirportOrigin = inBoundAirportOrigin;
            this.inBoundAirportDestination = inBoundAirportDestination;
            this.inBoundDepartureTime = inBoundDepartureTime;
            this.inBoundArrivalTime = inBoundArrivalTime;
            this.inBoundjourneyType = inBoundjourneyType;
            this.inBoundJourneyDuration = inBoundJourneyDuration;
            this.inBoundcarriers = inBoundcarriers;
            this.inBoundFlightURL =inBoundFlightURL;
            this.inBoundFlightName = inBoundcarriers.stream().collect(Collectors.joining(","));
        }

        public String getInBoundAirportOrigin() {
            return inBoundAirportOrigin;
        }

        public String getInBoundAirportDestination() {
            return inBoundAirportDestination;
        }

        public String getInBoundDepartureTime() {
            return inBoundDepartureTime;
        }

        public String getInBoundArrivalTime() {
            return inBoundArrivalTime;
        }

        public String getInBoundjourneyType() {
            return inBoundjourneyType;
        }

        public String getInBoundJourneyDuration() {
            return durationToHoursMinutes(inBoundJourneyDuration);
        }

        public List<String> getInBoundcarriers() {
            return inBoundcarriers;
        }

        public String getInBoundFlightName() {
            return inBoundFlightName;
        }
    }

    public static class Outbound {
        String outBoundAirportOrigin;
        String outBoundAirportDestination;
        String outBoundDepartureTime;
        String outBoundArrivalTime;
        String outBoundjourneyType;
        String outBoundJourneyDuration;
        List<String> outBoundcarriers;
        String outBoundFlightName;
        String outBoundFlightURL;

        public String getOutBoundFlightURL() {
            return outBoundFlightURL;
        }


        public Outbound(String outBoundAirportOrigin, String outBoundAirportDestination, String outBoundDepartureTime, String outBoundArrivalTime,
                        String outBoundjourneyType, String outBoundJourneyDuration, String outBoundFlightURL,List<String> outBoundcarriers) {
            this.outBoundAirportOrigin = outBoundAirportOrigin;
            this.outBoundAirportDestination = outBoundAirportDestination;
            this.outBoundDepartureTime = outBoundDepartureTime;
            this.outBoundArrivalTime = outBoundArrivalTime;
            this.outBoundjourneyType = outBoundjourneyType;
            this.outBoundJourneyDuration = outBoundJourneyDuration;
            this.outBoundcarriers = outBoundcarriers;
            this.outBoundFlightURL = outBoundFlightURL;
            this.outBoundFlightName = outBoundcarriers.stream().collect(Collectors.joining(","));
        }

        public String getOutBoundAirportOrigin() {
            return outBoundAirportOrigin;
        }

        public String getOutBoundAirportDestination() {
            return outBoundAirportDestination;
        }

        public String getOutBoundDepartureTime() {
            return outBoundDepartureTime;
        }

        public String getOutBoundArrivalTime() {
            return outBoundArrivalTime;
        }

        public String getOutBoundjourneyType() {
            return outBoundjourneyType;
        }

        public String getOutBoundJourneyDuration() {

            return durationToHoursMinutes(outBoundJourneyDuration);
        }

        public List<String> getOutBoundcarriers() {
            return outBoundcarriers;
        }

        public String getOutBoundFlightName() {
            return outBoundFlightName;
        }
    }

    Inbound inbound;
    Outbound outbound;
    String rating;

    String tripCost;
    String tripProviderAgent;


    public Inbound getInbound() {
        return inbound;
    }

    public Outbound getOutbound() {
        return outbound;
    }

    public String getRating() {
        return rating;
    }

    public String getTripCost() {
        return tripCost;
    }

    public String getTripProviderAgent() {
        return tripProviderAgent;
    }

    public DataModel(Outbound outbound, Inbound inbound, String rating, String tripCost, String tripProviderAgent) {
        this.inbound = inbound;
        this.outbound = outbound;
        this.rating = rating;
        this.tripCost = tripCost;
        this.tripProviderAgent = tripProviderAgent;
    }

    public static String durationToHoursMinutes(String duration)
    {
        if(duration == null || duration =="")
        {
            return duration;
        }
        int time = Integer.parseInt(duration);
        int hours = time / 60;
        int minutes = time % 60;
        return String.format("%dh %02dm", hours, minutes);
    }
}
