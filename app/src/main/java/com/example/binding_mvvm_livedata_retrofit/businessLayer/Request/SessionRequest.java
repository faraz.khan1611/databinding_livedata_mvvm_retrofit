package com.example.binding_mvvm_livedata_retrofit.businessLayer.Request;

public class SessionRequest {
//    country: UK
//    currency: GBP
//    locale: en-GB
//    locationSchema: sky
//    apikey: ss630745725358065467897349852985
//    originplace: EDI-sky
//    destinationplace: LOND-sky
//    outbounddate: 2019-03-22
//    inbounddate: 2019-03-29
//    adults: 1
//    children: 0
//    infants: 0
//    cabinclass: Economy


    public SessionRequest()
    {

    }

    public SessionRequest(String country, String currency, String locale, String locationSchema, String apikey, String originplace, String destinationplace, String outbounddate, String inbounddate, int adults, int children, int infants, String cabinclass) {
        this.country = country;
        this.currency = currency;
        this.locale = locale;
        this.locationSchema = locationSchema;
        this.apikey = apikey;
        this.originplace = originplace;
        this.destinationplace = destinationplace;
        this.outbounddate = outbounddate;
        this.inbounddate = inbounddate;
        this.adults = adults;
        this.children = children;
        this.infants = infants;
        this.cabinclass = cabinclass;
    }

    private String country;
    private String currency;
    private String locale;
    private String locationSchema;
    private String apikey;
    private String originplace;
    private String destinationplace;
    private String outbounddate;
    private String inbounddate;
    private int adults;
    private int children;
    private int infants;
    private String cabinclass;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getLocationSchema() {
        return locationSchema;
    }

    public void setLocationSchema(String locationSchema) {
        this.locationSchema = locationSchema;
    }

    public String getApikey() {
        return apikey;
    }

    public void setApikey(String apikey) {
        this.apikey = apikey;
    }

    public String getOriginplace() {
        return originplace;
    }

    public void setOriginplace(String originplace) {
        this.originplace = originplace;
    }

    public String getDestinationplace() {
        return destinationplace;
    }

    public void setDestinationplace(String destinationplace) {
        this.destinationplace = destinationplace;
    }

    public String getOutbounddate() {
        return outbounddate;
    }

    public void setOutbounddate(String outbounddate) {
        this.outbounddate = outbounddate;
    }

    public String getInbounddate() {
        return inbounddate;
    }

    public void setInbounddate(String inbounddate) {
        this.inbounddate = inbounddate;
    }

    public int getAdults() {
        return adults;
    }

    public void setAdults(int adults) {
        this.adults = adults;
    }

    public int getChildren() {
        return children;
    }

    public void setChildren(int children) {
        this.children = children;
    }

    public int getInfants() {
        return infants;
    }

    public void setInfants(int infants) {
        this.infants = infants;
    }

    public String getCabinclass() {
        return cabinclass;
    }

    public void setCabinclass(String cabinclass) {
        this.cabinclass = cabinclass;
    }
}
