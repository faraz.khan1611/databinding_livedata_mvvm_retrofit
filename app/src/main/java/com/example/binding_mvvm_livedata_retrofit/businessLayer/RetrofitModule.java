package com.example.binding_mvvm_livedata_retrofit.businessLayer;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

class RetrofitModule {

    OkHttpClient httpClient = new OkHttpClient.Builder().readTimeout(30,TimeUnit.SECONDS).build();
//    Gson gson = new GsonBuilder()
//            .registerTypeAdapter(SessionResponse.class, new SessionDeserializer<>(SessionResponse.class, "ServiceQuery")).create();

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(APIUrl.BASE_URL)
            .client(httpClient)
//            .addConverterFactory(GsonConverterFactory.create(gson))
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
