package com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling;

public abstract class GenericClass<U> {

    public abstract U getId();
}
