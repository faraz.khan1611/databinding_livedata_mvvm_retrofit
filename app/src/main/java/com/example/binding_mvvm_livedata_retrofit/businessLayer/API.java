package com.example.binding_mvvm_livedata_retrofit.businessLayer;

import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.SessionResponse;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling.PollingResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

interface API {

    @FormUrlEncoded
    @POST("/apiservices/pricing/v1.0")
    Call<SessionResponse> getSkySessionURL(@Field("apikey") String apikey, @Field("country") String country, @Field("currency") String currency,
                                           @Field("locale") String locale, @Field("locationSchema") String locationSchema, @Field("originplace") String originplace,
                                           @Field("destinationplace") String destinationplace, @Field("outbounddate") String outbounddate, @Field("inbounddate") String inbounddate,
                                           @Field("adults") int adults, @Field("children") int children, @Field("infants") int infants, @Field("cabinclass") String cabinclass);



    @GET
    Call<PollingResponse> getPollingResult(@Url String url, @Query("apikey") String apiKey);
}
