package com.example.binding_mvvm_livedata_retrofit.businessLayer.Response;

import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

public class SessionDeserializer<T> implements JsonDeserializer<T>  {

        private Class<T> mClass;
        private String mKey;

        public SessionDeserializer(Class<T> targetClass, String key) {
            mClass = targetClass;
            mKey = key;
        }

        @Override
        public T deserialize(JsonElement je, Type type, JsonDeserializationContext jdc)
                throws JsonParseException {
            JsonElement json = je.getAsJsonObject().get(mKey);
            return new Gson().fromJson(json, mClass);

        }
}
