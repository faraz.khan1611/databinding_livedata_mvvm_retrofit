package com.example.binding_mvvm_livedata_retrofit.businessLayer;

import android.arch.lifecycle.MutableLiveData;

import com.example.binding_mvvm_livedata_retrofit.binding.ApiStatusDataModel;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling.GenericClass;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.SessionResponse;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling.PollingResponse;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.SkyResponse;

import java.util.Hashtable;
import java.util.List;

import okhttp3.Headers;

//This class manages were to get data from, network, disk or Web.

public class Repository implements ApiInterfaceCallback<SkyResponse> {


    MutableLiveData<PollingResponse> mLiveData = new MutableLiveData<PollingResponse>();
    MutableLiveData<ApiStatusDataModel> mStatusLiveData = new MutableLiveData<ApiStatusDataModel>();

    private WebService mWebService;
    private static class Singleton
    {
        private static final Repository INSTANCE = new Repository();
    }

    public static Repository getInstance()
    {
        return Repository.Singleton.INSTANCE;
    }

    private Repository()
    {
        mWebService = WebService.getInstance();
        mWebService.setWebServiceListener(this);
    }

    public void makeSessionRequest(String country,String currency,String locale,String locationSchema,String originplace,
                                   String destinationplace,String outbounddate, String inbounddate,int adults,int children,int infant,
                                   String cabinClass)
    {

        mWebService.makeSessionRequest(country,currency, locale,locationSchema,originplace,
                destinationplace,outbounddate, inbounddate, adults, children, infant, cabinClass);
    }

    public MutableLiveData<PollingResponse> getPollingSessionLiveData() {
        return mLiveData;
    }

    public MutableLiveData<ApiStatusDataModel> getAPIStatusData() {
        return mStatusLiveData;
    }


    // Used Generics Here to minimize code and increase readability
    //    private Hashtable<String, LegsItem> legTable;
    //
    //    private Hashtable<String, AgentsItem> agentTable;
    //
    //    private Hashtable<String, SegmentsItem> SegmentTable;
    //
    //    private Hashtable<String, PlacesItem> carrierTable;
    //
    //    private Hashtable<String, CarriersItem> placeTable;

    public <U, T extends GenericClass<U>> Hashtable<U, T> prepareTableUsingGenerics(List<T> list) {
        Hashtable<U, T> table = new Hashtable();
        for (T item : list) {
            table.put(item.getId(), item);
        }

        return table;
    }

    @Override
    public void onWebServiceResponseSuccess(SkyResponse response, Headers headers) {
        if (response instanceof SessionResponse) {
        }
        else if (response instanceof PollingResponse) {

            //Preparing hashtable using generics
            PollingResponse resp = (PollingResponse) response;
            resp.setLegTable(prepareTableUsingGenerics(resp.getLegs()));
            resp.setAgentTable(prepareTableUsingGenerics(resp.getAgents()));
            resp.setSegmentTable(prepareTableUsingGenerics(resp.getSegments()));
            resp.setPlaceTable(prepareTableUsingGenerics(resp.getPlaces()));
            resp.setCarrierTable(prepareTableUsingGenerics(resp.getCarriers()));

            mLiveData.setValue((PollingResponse) response);
            mStatusLiveData.setValue(new ApiStatusDataModel(true, ""));

            //Done now inform LiveData to prepare view
        }
        else
        {

        }
    }


    @Override
    public void onWebServiceFailure(String errorMessage) {
        mStatusLiveData.setValue(new ApiStatusDataModel(false,errorMessage));
    }

}
