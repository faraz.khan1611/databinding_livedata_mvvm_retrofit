package com.example.binding_mvvm_livedata_retrofit.businessLayer;
import okhttp3.Headers;

interface ApiInterfaceCallback<T> {
    void onWebServiceResponseSuccess(T response, Headers headers);

    void onWebServiceFailure(String errorMessage);
}
