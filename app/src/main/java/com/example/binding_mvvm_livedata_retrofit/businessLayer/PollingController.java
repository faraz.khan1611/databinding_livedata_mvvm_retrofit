package com.example.binding_mvvm_livedata_retrofit.businessLayer;

import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling.PollingResponse;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.SkyResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class PollingController implements Callback<PollingResponse> {

    ApiInterfaceCallback<SkyResponse> listener;
    public void makePollingCall(API apiService, String URL, String API, ApiInterfaceCallback<SkyResponse> listener)
    {
        this.listener = listener;
        apiService.getPollingResult(URL,API).enqueue(this);
    }

    @Override
    public void onResponse(Call<PollingResponse> call, Response<PollingResponse> response) {
        if(this.listener != null)
        {
            if(response.isSuccessful()) {
                this.listener.onWebServiceResponseSuccess((SkyResponse) response.body(), response.headers());
            }
            else
            {
                this.listener.onWebServiceFailure("Something went wrong!!");
            }
        }
    }

    @Override
    public void onFailure(Call<PollingResponse> call, Throwable t) {
        if(this.listener != null)
        {
            this.listener.onWebServiceFailure(t.getMessage());
        }
    }
}
