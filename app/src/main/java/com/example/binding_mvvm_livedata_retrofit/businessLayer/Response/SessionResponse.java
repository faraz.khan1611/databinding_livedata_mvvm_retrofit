package com.example.binding_mvvm_livedata_retrofit.businessLayer.Response;

import com.google.gson.annotations.SerializedName;

public class SessionResponse extends SkyResponse{
	@SerializedName("ServiceQuery")
	private ServiceQuery serviceQuery;

	public void setServiceQuery(ServiceQuery serviceQuery){
		this.serviceQuery = serviceQuery;
	}

	public ServiceQuery getServiceQuery(){
		return serviceQuery;
	}

	@Override
 	public String toString(){
		return 
			"SessionResponse{" + 
			"ServiceQuery = '" + serviceQuery + '\'' +
			"}";
		}
}
