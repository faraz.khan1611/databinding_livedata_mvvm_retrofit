package com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling;


import com.google.gson.annotations.SerializedName;

public class PlacesItem extends GenericClass{

	@SerializedName("ParentId")
	private int parentId;

	@SerializedName("Type")
	private String type;

	@SerializedName("Id")
	private String id;

	@SerializedName("Code")
	private String code;

	@SerializedName("Name")
	private String name;

	public void setParentId(int parentId){
		this.parentId = parentId;
	}

	public int getParentId(){
		return parentId;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setId(String id){
		this.id = id;
	}

	public Integer getId(){
		return Integer.parseInt(id);
	}

	public void setCode(String code){
		this.code = code;
	}

	public String getCode(){
		return code;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	@Override
 	public String toString(){
		return 
			"PlacesItem{" + 
			"parentId = '" + parentId + '\'' + 
			",type = '" + type + '\'' + 
			",id = '" + id + '\'' + 
			",code = '" + code + '\'' + 
			",name = '" + name + '\'' + 
			"}";
		}

//	@Override
//	public String getIDKey() {
//		return id;
//	}
}