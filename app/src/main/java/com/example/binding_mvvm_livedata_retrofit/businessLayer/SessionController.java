package com.example.binding_mvvm_livedata_retrofit.businessLayer;

import com.example.binding_mvvm_livedata_retrofit.businessLayer.Request.SessionRequest;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.SessionResponse;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.SkyResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

class SessionController implements Callback<SessionResponse> {
    ApiInterfaceCallback<SkyResponse> listener;

    public void makeSessionCall(API apiService, SessionRequest request, ApiInterfaceCallback<SkyResponse> listener)
    {
        this.listener = listener;
        apiService.getSkySessionURL(request.getApikey(),request.getCountry(),request.getCurrency(),
                request.getLocale(),request.getLocationSchema(),request.getOriginplace(),
                request.getDestinationplace(),request.getOutbounddate(),request.getInbounddate(),
                request.getAdults(),request.getChildren(),request.getInfants(),request.getCabinclass()).enqueue(this);
    }

    @Override
    public void onResponse(Call<SessionResponse> call, Response<SessionResponse> response) {

        if(this.listener != null)
        {
            if(response.isSuccessful()) {
                this.listener.onWebServiceResponseSuccess((SkyResponse) response.body(), response.headers());
            }
            else
            {
                this.listener.onWebServiceFailure("Something went wrong!!");
            }
        }
    }

    @Override
    public void onFailure(Call<SessionResponse> call, Throwable t) {
        if(this.listener != null)
        {
           this.listener.onWebServiceFailure(t.getMessage());
        }
    }

}
