package com.example.binding_mvvm_livedata_retrofit.businessLayer;

import com.example.binding_mvvm_livedata_retrofit.businessLayer.Request.SessionRequest;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.SessionResponse;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling.PollingResponse;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.SkyResponse;

import okhttp3.Headers;

class WebService implements ApiInterfaceCallback<SkyResponse> {

    private String API_KEY = "ss630745725358065467897349852985";
    private String TAG = "WebService";
    private API apiService;
    ApiInterfaceCallback listener;

    private static class Singleton {
        private static final WebService INSTANCE = new WebService();
    }

    public static WebService getInstance() {
        return Singleton.INSTANCE;
    }

    private WebService() {
        apiService = new RetrofitModule().retrofit.create(API.class);
    }

    public void setWebServiceListener(ApiInterfaceCallback listener) {
        this.listener = listener;
    }

    public void makeSessionRequest(String country,String currency,String locale,String locationSchema,String originplace,
                                   String destinationplace,String outbounddate, String inbounddate,int adults,int children,int infant,
                                   String cabinClass) {
        SessionRequest request = new SessionRequest();
        request.setCountry(country);
        request.setCurrency(currency);
        request.setLocale(locale);
        request.setLocationSchema(locationSchema);
        request.setApikey(API_KEY);
        request.setOriginplace(originplace);
        request.setDestinationplace(destinationplace);
        request.setOutbounddate(outbounddate);
        request.setInbounddate(inbounddate);
        request.setAdults(adults);
        request.setChildren(children);
        request.setInfants(infant);
        request.setCabinclass(cabinClass);
        new SessionController().makeSessionCall(apiService, request, this);

    }

    public void makePollingRequest(String URL) {
        new PollingController().makePollingCall(apiService, URL, API_KEY, this);
    }



    //Web service interface
    @Override
    public void onWebServiceResponseSuccess(SkyResponse response, Headers headers) {

        if (response instanceof SessionResponse) {
//            Log.d(TAG, "SessionResponse::::" + response);
            if (headers.get("Location") != null) {
                makePollingRequest(headers.get("Location"));
            }
            else
            {
                listener.onWebServiceFailure("Location Header not found");
            }
        } else if (response instanceof PollingResponse) {
//            Log.d(TAG, "PollingResponse::::" + response);
        }

        if(listener != null)
        {
            listener.onWebServiceResponseSuccess(response,headers);
        }
    }

    @Override
    public void onWebServiceFailure(String errorMessage) {

//        Log.d(TAG, "Failure::::" + errorMessage);
        if(listener != null)
        {
            listener.onWebServiceFailure(errorMessage);
        }

    }
}
