package com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.Polling;

import java.util.Hashtable;
import java.util.List;

import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.ServiceQuery;
import com.example.binding_mvvm_livedata_retrofit.businessLayer.Response.SkyResponse;
import com.google.gson.annotations.SerializedName;

public class PollingResponse extends SkyResponse {

	@SerializedName("Status")
	private String status;

	@SerializedName("Carriers")
	private List<CarriersItem> carriers;

	@SerializedName("Legs")
	private List<LegsItem> legs;

	@SerializedName("Itineraries")
	private List<ItinerariesItem> itineraries;

	@SerializedName("Query")
	private Query query;

	@SerializedName("SessionKey")
	private String sessionKey;

	@SerializedName("Agents")
	private List<AgentsItem> agents;

	@SerializedName("Segments")
	private List<SegmentsItem> segments;

	@SerializedName("Currencies")
	private List<CurrenciesItem> currencies;

	@SerializedName("Places")
	private List<PlacesItem> places;

	@SerializedName("ServiceQuery")
	private ServiceQuery serviceQuery;

	private Hashtable<String,LegsItem> legTable;

	private Hashtable<Integer,AgentsItem> agentTable;

	private Hashtable<Integer,SegmentsItem> SegmentTable;

	private Hashtable<Integer,CarriersItem> carrierTable;

	private Hashtable<Integer,PlacesItem> placeTable;

	public Hashtable<String, LegsItem> getLegTable() {
		return legTable;
	}

	public void setLegTable(Hashtable<String, LegsItem> legTable) {
		this.legTable = legTable;
	}

	public Hashtable<Integer, AgentsItem> getAgentTable() {
		return agentTable;
	}

	public void setAgentTable(Hashtable<Integer, AgentsItem> agentTable) {
		this.agentTable = agentTable;
	}

	public Hashtable<Integer, SegmentsItem> getSegmentTable() {
		return SegmentTable;
	}

	public void setSegmentTable(Hashtable<Integer, SegmentsItem> segmentTable) {
		SegmentTable = segmentTable;
	}

	public Hashtable<Integer, CarriersItem> getCarrierTable() {
		return carrierTable;
	}

	public void setCarrierTable(Hashtable<Integer, CarriersItem> carrierTable) {
		this.carrierTable = carrierTable;
	}

	public Hashtable<Integer, PlacesItem> getPlaceTable() {
		return placeTable;
	}

	public void setPlaceTable(Hashtable<Integer, PlacesItem> placeTable) {
		this.placeTable = placeTable;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setCarriers(List<CarriersItem> carriers){
		this.carriers = carriers;
	}

	public List<CarriersItem> getCarriers(){
		return carriers;
	}

	public void setLegs(List<LegsItem> legs){
		this.legs = legs;
	}

	public List<LegsItem> getLegs(){
		return legs;
	}

	public void setItineraries(List<ItinerariesItem> itineraries){
		this.itineraries = itineraries;
	}

	public List<ItinerariesItem> getItineraries(){
		return itineraries;
	}

	public void setQuery(Query query){
		this.query = query;
	}

	public Query getQuery(){
		return query;
	}

	public void setSessionKey(String sessionKey){
		this.sessionKey = sessionKey;
	}

	public String getSessionKey(){
		return sessionKey;
	}

	public void setAgents(List<AgentsItem> agents){
		this.agents = agents;
	}

	public List<AgentsItem> getAgents(){
		return agents;
	}

	public void setSegments(List<SegmentsItem> segments){
		this.segments = segments;
	}

	public List<SegmentsItem> getSegments(){
		return segments;
	}

	public void setCurrencies(List<CurrenciesItem> currencies){
		this.currencies = currencies;
	}

	public List<CurrenciesItem> getCurrencies(){
		return currencies;
	}

	public void setPlaces(List<PlacesItem> places){
		this.places = places;
	}

	public List<PlacesItem> getPlaces(){
		return places;
	}

	public void setServiceQuery(ServiceQuery serviceQuery){
		this.serviceQuery = serviceQuery;
	}

	public ServiceQuery getServiceQuery(){
		return serviceQuery;
	}

	@Override
 	public String toString(){
		return 
			"PollingResponse{" +
			"status = '" + status + '\'' + 
			",carriers = '" + carriers + '\'' + 
			",legs = '" + legs + '\'' + 
			",itineraries = '" + itineraries + '\'' + 
			",query = '" + query + '\'' + 
			",sessionKey = '" + sessionKey + '\'' + 
			",agents = '" + agents + '\'' + 
			",segments = '" + segments + '\'' + 
			",currencies = '" + currencies + '\'' + 
			",places = '" + places + '\'' + 
			",serviceQuery = '" + serviceQuery + '\'' + 
			"}";
		}
}