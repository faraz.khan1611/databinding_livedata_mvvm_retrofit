package com.example.binding_mvvm_livedata_retrofit.ui;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.binding_mvvm_livedata_retrofit.binding.DataModel;
import com.example.binding_mvvm_livedata_retrofit.R;
import com.example.binding_mvvm_livedata_retrofit.databinding.ItemIternaryRowBinding;

import java.util.ArrayList;
import java.util.List;
import com.example.binding_mvvm_livedata_retrofit.BR;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private List<DataModel> dataModelList = new ArrayList<DataModel>();
    private Context context;

    public RecyclerViewAdapter(Context ctx)
    {
        this.context = ctx;
    }

    public RecyclerViewAdapter(List<DataModel> dataModelList, Context ctx) {
        this.dataModelList = dataModelList;
        context = ctx;
    }

    public void loadSkyModel(List<DataModel> list)
    {
        dataModelList.clear();
        dataModelList.addAll(list);
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
        ItemIternaryRowBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_iternary_row, parent, false);

        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DataModel dataModel = dataModelList.get(position);
        holder.bind(dataModel);
    }


    @Override
    public int getItemCount() {
        return dataModelList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemIternaryRowBinding itemRowBinding;

        public ViewHolder(ItemIternaryRowBinding itemRowBinding) {
            super(itemRowBinding.getRoot());
            this.itemRowBinding = itemRowBinding;
        }

        public void bind(Object obj) {
            itemRowBinding.setVariable(BR.model,obj);
            itemRowBinding.executePendingBindings();
        }
    }
}