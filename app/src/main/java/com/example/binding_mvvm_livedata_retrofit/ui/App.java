package com.example.binding_mvvm_livedata_retrofit.ui;

import android.app.Application;
import android.content.Context;

public class App extends Application {
    public static Context appContext;

    @Override public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();
    }

    public static Context getAppContext() {
        return appContext;
    }
}
