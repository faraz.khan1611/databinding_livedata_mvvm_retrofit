package com.example.binding_mvvm_livedata_retrofit.ui;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.example.binding_mvvm_livedata_retrofit.binding.ApiStatusDataModel;
import com.example.binding_mvvm_livedata_retrofit.binding.DataModel;
import com.example.binding_mvvm_livedata_retrofit.R;
import com.example.binding_mvvm_livedata_retrofit.viewModel.VM;
import com.example.binding_mvvm_livedata_retrofit.databinding.ActivityFlightBinding;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityFlightBinding mActivityFlightBinding;
    private VM mViewModel;
    RecyclerViewAdapter mRecyclerViewAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivityFlightBinding = DataBindingUtil.setContentView(this, R.layout.activity_flight);


        mViewModel = ViewModelProviders.of(this).get(VM.class);
        mRecyclerViewAdapter = new RecyclerViewAdapter(this);
        mActivityFlightBinding.setSkyRecycleViewAdapter(mRecyclerViewAdapter);
        initObservers();
        makeSessionRequest();

        getSupportActionBar().setTitle("EDI - LOND");

        SimpleDateFormat sm = new SimpleDateFormat("dd MMM");
        Date nextMonday = getNextMonday();
        String subtitle = sm.format(nextMonday) + " - " + sm.format(getNextDateFor(nextMonday));
        getSupportActionBar().setSubtitle(subtitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void initObservers() {

        mViewModel.getPollingLiveData().observe(this, new Observer<List<DataModel>>() {
            @Override
            public void onChanged(@Nullable List<DataModel> dataModels) {
                mRecyclerViewAdapter.loadSkyModel(dataModels);
                mActivityFlightBinding.resultcount.setText(String.format("%d results",dataModels.size()));
                mActivityFlightBinding.progressBar.setVisibility(View.GONE);
                mActivityFlightBinding.loading.setVisibility(View.GONE);
            }
        });

        //Below API status track failure
        mViewModel.getAPIStatus().observe(this, new Observer<ApiStatusDataModel>() {
            @Override
            public void onChanged(@Nullable ApiStatusDataModel dataModels) {
                if(dataModels.getSuccess()) {
                }
                else
                {
                    mActivityFlightBinding.progressBar.setVisibility(View.GONE);
                    mActivityFlightBinding.loading.setText("Fail to load iternaries");
                    Toast.makeText(getApplicationContext(), dataModels.getMessage(),
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void makeSessionRequest()
    {
        Date nextMonday = getNextMonday();
        Date returnDate = getNextDateFor(nextMonday);

        SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd");

        mViewModel.makeSessionRequest("UK","GBP","en-GB","sky","EDI-sky",
                "LOND-sky",sm.format(nextMonday),sm.format(returnDate),1,0,0,"Economy");
    }

    private Date getNextMonday()
    {
        Calendar now = Calendar.getInstance();
        int weekday = now.get(Calendar.DAY_OF_WEEK);
        if (weekday != Calendar.MONDAY)
        {
            int days = (Calendar.SATURDAY - weekday + 2) % 7;
            now.add(Calendar.DAY_OF_YEAR, days);
        }
        return now.getTime();
    }

    private Date getNextDateFor(Date date)
    {
        final Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        return calendar.getTime();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
