** uses android.arch framework**

The first page displays flight iternaries using sky api, it merges multiple iternaries and shows the best available route.

## About the project
App Make use of  LiveData, Retrofit, Data binding, MVVM to make a main travel app page.

The response contains a list of flight iternaries.



## Notes
1. The app uses LiveData for lifecycle aware callbacks.
2. THe app uses MVVM design 
3. The app combines data from multiple agents and show the minimum of all of them

## Screenshots

![Webp.net-resizeimage__5_](/uploads/9820cb8d8317b64d5ef2946add225e57/Webp.net-resizeimage__5_.png)